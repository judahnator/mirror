<?php

declare(strict_types=1);

namespace judahnator\Mirror;

use Countable;
use IteratorAggregate;

/**
 * Class MirrorType
 * @package judahnator\Mirror
 * @phpstan-implements IteratorAggregate<int, mixed>
 */
abstract class MirrorType implements Countable, IteratorAggregate
{
    /**
     * Checks the type of an element and returns an Object or Array type where applicable.
     *
     * @param mixed $el
     * @return ArrayType|ObjectType|mixed
     */
    final public static function of(&$el)
    {
        if (is_object($el)) {
            return new ObjectType($el);
        } elseif (is_array($el)) {
            return new ArrayType($el);
        } else {
            return $el;
        }
    }
}
