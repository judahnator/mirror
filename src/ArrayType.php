<?php

declare(strict_types=1);

namespace judahnator\Mirror;

use ArrayAccess;
use InvalidArgumentException;

/**
 * Class ArrayType
 * @package judahnator\Mirror
 * @phpstan-implements ArrayAccess<int, mixed>
 */
class ArrayType extends MirrorType implements ArrayAccess
{
    /** @var array<int, mixed> */
    protected array $list;

    /**
     * ArrayType constructor.
     * @param array<int, mixed> $list
     */
    public function __construct(array &$list)
    {
        if (!empty(array_filter(array_keys($list), 'is_string'))) {
            throw new InvalidArgumentException('Cannot use an associative array as a list type.');
        }
        $this->list =& $list;
    }

    public function count(): int
    {
        return count($this->list);
    }

    /**
     * @param int|null $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        if (is_null($offset)) {
            return false;
        } elseif (!is_int($offset)) {
            throw new InvalidArgumentException('Illegal offset type.');
        }
        return array_key_exists($offset, $this->list);
    }

    /**
     * @param int|null $offset
     * @return ArrayType|ObjectType|mixed|null
     */
    public function offsetGet($offset)
    {
        if (!is_int($offset)) {
            if (!is_null($offset)) {
                throw new InvalidArgumentException('Illegal offset type.');
            }
            return null;
        }
        if (!array_key_exists($offset, $this->list)) {
            return null;
        }
        return static::of($this->list[$offset]);
    }

    public function offsetSet($offset, $value)
    {
        if (is_numeric($offset)) {
            $this->list[$offset] = $value;
        } elseif (is_null($offset)) {
            $this->list[] = $value;
        } else {
            throw new InvalidArgumentException('Illegal offset type.');
        }
        $this->list = array_values($this->list);
    }

    public function offsetUnset($offset): void
    {
        if (!is_numeric($offset)) {
            throw new InvalidArgumentException('Illegal offset type.');
        }
        if (array_key_exists($offset, $this->list)) {
            unset($this->list[$offset]);
        }
        $this->list = array_values($this->list);
    }

    /**
     * @return \Generator<int, mixed>
     */
    public function getIterator()
    {
        foreach ($this->list as $k => &$v) {
            yield $k => static::of($v);
        }
    }
}
