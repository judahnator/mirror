<?php

declare(strict_types=1);

namespace judahnator\Mirror;

class ObjectType extends MirrorType
{
    protected object $map;

    public function __construct(object &$map)
    {
        $this->map = $map;
    }

    /**
     * @param string|null $name
     * @return ArrayType|ObjectType|mixed|null
     */
    public function __get(?string $name)
    {
        if (is_null($name) || !property_exists($this->map, $name)) {
            return null;
        }
        return static::of($this->map->{$name});
    }

    public function __isset(?string $name): bool
    {
        return !is_null($name) && property_exists($this->map, $name);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value): void
    {
        $this->map->{$name} = $value;
    }

    public function __unset(string $name): void
    {
        if (property_exists($this->map, $name)) {
            unset($this->map->{$name});
        }
    }

    public function count(): int
    {
        return count(get_object_vars($this->map));
    }

    public function getIterator()
    {
        // @phpstan-ignore-next-line
        foreach ($this->map as $k => &$v) {
            yield $k => static::of($v);
        }
    }
}
