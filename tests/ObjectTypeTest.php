<?php

declare(strict_types=1);

namespace judahnator\Mirror\Tests;

use judahnator\Mirror\MirrorType;
use judahnator\Mirror\ObjectType;
use PHPUnit\Framework\TestCase;

/**
 * Class ObjectTypeTest
 * @package judahnator\Mirror\Tests
 * @covers \judahnator\Mirror\ObjectType
 * @uses \judahnator\Mirror\MirrorType::of()
 */
final class ObjectTypeTest extends TestCase
{
    public function testCountingValues(): void
    {
        $el = (object)['foo' => 'bar', 'bing' => 'baz'];
        $this->assertCount(count(get_object_vars($el)), new ObjectType($el));
    }

    public function testCreatingObjectType(): void
    {
        $el = (object)['foo' => 'bar'];
        $this->assertInstanceOf(ObjectType::class, MirrorType::of($el));
    }

    public function testObjectTypeUsage(): void
    {
        $el = (object)['foo' => 'bar', 'bing' => 'baz'];
        $map = MirrorType::of($el);

        // ensure we have all expected values
        foreach ($el as $k => $v) {
            $this->assertTrue(isset($map->{$k}));
            $this->assertEquals($v, $map->{$k});
        }

        // ensure nothing strange happened
        foreach ($map as $k => $v) {
            $this->assertEquals($el->{$k}, $v);
        }
    }

    /**
     * @uses \judahnator\Mirror\ArrayType
     */
    public function testObjectIteration(): void
    {
        $el = (object)['first' => [], 'second' => []];
        $map = new ObjectType($el);

        foreach ($map as $k => $v) {
            $v[] = $k;
        }

        $this->assertEquals((object)['first' => ['first'], 'second' => ['second']], $el);
    }
}
