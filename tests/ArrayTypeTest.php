<?php

declare(strict_types=1);

namespace judahnator\Mirror\Tests;

use InvalidArgumentException;
use judahnator\Mirror\ArrayType;
use judahnator\Mirror\MirrorType;
use PHPUnit\Framework\TestCase;

/**
 * Class ArrayTypeTest
 * @package judahnator\Mirror\Tests
 * @covers \judahnator\Mirror\ArrayType
 * @uses \judahnator\Mirror\MirrorType::of
 */
final class ArrayTypeTest extends TestCase
{
    public function testCountingValues(): void
    {
        $el = ['foo', 'bar'];
        $this->assertCount(count($el), new ArrayType($el));
    }

    public function testCreatingArrayType(): void
    {
        $el = ['foo', 'bar'];
        $this->assertInstanceOf(ArrayType::class, ArrayType::of($el));
    }

    public function testFailureWhenCreatingInvalidArrayType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Cannot use an associative array as a list type.');
        $el = ['foo' => 'bar'];
        MirrorType::of($el);
    }

    public function testArrayTypeUsage(): void
    {
        $el = ['foo', 'bar', 'baz'];
        $map = new ArrayType($el);

        // Ensure all expected values are present
        foreach ($el as $k => $v) {
            $this->assertTrue(isset($map[$k]));
            $this->assertEquals($v, $map[$k]);
        }

        // Ensure nothing strange happened
        foreach ($map as $k => $v) {
            $this->assertEquals($el[$k], $v);
        }
    }

    public function testUnsettingValues(): void
    {
        // Covers a case where the keys would be preserved
        $el = ['foo', 'bar', 'baz'];
        $arr = ArrayType::of($el);
        unset($arr[1]);
        $this->assertEquals('baz', $el[1]);
        $this->assertEquals('baz', $arr[1]);
    }
}
