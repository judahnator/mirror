<?php

declare(strict_types=1);

namespace judahnator\Mirror\Tests;

use judahnator\Mirror\ArrayType;
use judahnator\Mirror\MirrorType;
use judahnator\Mirror\ObjectType;
use PHPUnit\Framework\TestCase;

/**
 * Class MirrorTypeTest
 * @package judahnator\Mirror\Tests
 * @covers \judahnator\Mirror\MirrorType
 * @uses \judahnator\Mirror\ArrayType
 * @uses \judahnator\Mirror\ObjectType
 */
final class MirrorTypeTest extends TestCase
{
    public function testComplexMirrorType(): void
    {
        $el = (object)[
            'foo' => [
                'bar',
                'baz',
            ],
        ];
        $mirror = MirrorType::of($el);
        $this->assertInstanceOf(ObjectType::class, $mirror);
        $this->assertInstanceOf(ArrayType::class, $mirror->foo);
        $this->assertEquals('bar', $mirror->foo[0]);
        $this->assertEquals('baz', $mirror->foo[1]);
    }

    public function testUpdatingValues(): void
    {
        $el = (object)[
            'foo' => [
                'bar',
                'baz',
            ],
        ];
        $mirror = MirrorType::of($el);
        $mirror->bing = 'bong';
        $foo = $mirror->foo;
        $foo[] = 'foobar';
        $foo[0] .= $foo[1];
        $this->assertEquals(
            (object)[
                'foo' => [
                    'barbaz',
                    'baz',
                    'foobar',
                ],
                'bing' => 'bong'
            ],
            $el
        );
    }
}
